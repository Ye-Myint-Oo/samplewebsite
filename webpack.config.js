const path = require('path');
const { GenerateSW } = require('workbox-webpack-plugin');

module.exports = {
  entry: './src/app/(tabs)/index.tsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'src'),
  },
  module: {
    rules: [
      {
          test: /\.js$/,
          exclude: ['/node_modules/', '/src/scss/'],
          use: [
              'babel-loader'
          ]
      },
      {
          test: /\.ts(x?)$/,
          exclude: ['/node_modules/', '/src/scss/'],
          use: [
              'babel-loader',
              'ts-loader',
          ]
      },
      {
          test:  /\.json$/,
          loader: 'json-loader'
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new GenerateSW({
      clientsClaim: true,
      skipWaiting: true,
    }),
  ],
  mode: 'production',
  // Other webpack configuration...
};
